# Heat

A REST API for registering of thermostats sensors and calculate and provide statistics.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

*  PostgreSQL
*  Redis
*  Bundler

### Installing

Install gem dependencies by running:

```
bundle
```

Configure your database credentials on the .env file:

```
# .env

DB_USERNAME=username
DB_PASSWOR=password
```

Setup your database:

```
rake db:setup
```

Start the application with foreman:

```
foreman start -f Procfile.dev
```

You should see something like this:

```
19:59:24 web.1    | started with pid 2044
19:59:24 worker.1 | started with pid 2045
19:59:25 web.1    | Puma starting in single mode...
19:59:25 web.1    | * Version 3.12.1 (ruby 2.6.3-p62), codename: Llamas in Pajamas
19:59:25 web.1    | * Min threads: 5, max threads: 5
19:59:25 web.1    | * Environment: development
19:59:25 web.1    | * Listening on tcp://0.0.0.0:5000
19:59:25 web.1    | Use Ctrl-C to stop
19:59:26 worker.1 | 2019-08-26T22:59:26.069Z 2048 TID-gqzkvktic INFO: Running in ruby 2.6.3p62 (2019-04-16 revision 67580) [x86_64-linux]
19:59:26 worker.1 | 2019-08-26T22:59:26.069Z 2048 TID-gqzkvktic INFO: See LICENSE and the LGPL-3.0 for licensing details.
19:59:26 worker.1 | 2019-08-26T22:59:26.069Z 2048 TID-gqzkvktic INFO: Upgrade to Sidekiq Pro for more features and support: http://sidekiq.org
19:59:26 worker.1 | 2019-08-26T22:59:26.069Z 2048 TID-gqzkvktic INFO: Booting Sidekiq 5.2.7 with redis options {:id=>"Sidekiq-server-PID-2048", :url=>nil}
19:59:26 worker.1 | 2019-08-26T22:59:26.070Z 2048 TID-gqzkvktic INFO: Starting processing, hit Ctrl-C to stop
```

Voilá!

## Running the tests

To run the tests use:

```
bundle exec rspec
```

## Live testing

You can live test a demo application using a api testing tool like [Postman](https://www.getpostman.com/) at: https://diguliu-heat.herokuapp.com/

Here are the available endpoints:
*  **GET** /api/v1/thermostat: retrieves every thermostat available
*  **GET** /api/v1/thermostat/stats (household\_token:string): retrieves the thermostat authenticated through household\_token stats
*  **GET** /api/v1/reading: retrieves every reading available
*  **POST** /api/v1/reading (household\_token:string, temperature:float, humidity:float, battery_charge:float): register a reading of sensors to a thermostat
*  **GET** /api/v1/reading/:tracking\_number (household\_tokan:string): retrieves a thermostat reading by tracking\_number


## Deployment

The only deployment available right now is via Gitlab CI/CD to a Heroku app using dpl. Look for further documentation at: https://github.com/travis-ci/dpl

## Built With

* [Rails](https://rubyonrails.org//) - The web framework used
* [Bundler](https://bundler.io/) - Dependency Management

## Authors

* **Rodrigo Souto** - [Gitlab](https://gitlab.com/diguliu)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
