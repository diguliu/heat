require 'rails_helper'

RSpec.describe Api::V1::ThermostatController, type: :controller do
  describe "GET #stats" do
    let(:thermostat) { Thermostat.create! }

    it "returns thermostat report" do
      Reading.scheduled_create!(
        thermostat: thermostat,
        temperature: 10,
        humidity: 20,
        battery_charge: 30
      )
      Reading.scheduled_create!(
        thermostat: thermostat,
        temperature: 20,
        humidity: 30,
        battery_charge: 10
      )
      Reading.scheduled_create!(
        thermostat: thermostat,
        temperature: 30,
        humidity: 10,
        battery_charge: 20
      )

      get :stats, params: {household_token: thermostat.household_token}

      expect(response).to have_http_status(:success)
      Stats.fields.each do |field|
        expect(parsed_response[field]['min']).to eq(10)
        expect(parsed_response[field]['max']).to eq(30)
        expect(parsed_response[field]['avg']).to eq(20)
      end
    end

    it 'warns missing household_token' do
      get :stats
      expect(response).to have_http_status(:unauthorized)
    end

    it 'warns invalid household_token' do
      get :stats, params: {household_token: 'invalid-token'}
      expect(response).to have_http_status(:not_found)
    end
  end
end
