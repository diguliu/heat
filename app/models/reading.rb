class Reading < ApplicationRecord
  belongs_to :thermostat
  validates_presence_of :thermostat

  # Retrieves cached version of a reading
  def self.get_cached(key)
    return unless key.present?

    parsed_reading = $redis.get(key)
    return unless parsed_reading.present?

    Reading.new(JSON.parse(parsed_reading))
  end

  def self.scheduled_create!(attrs)
    reading = Reading.new(attrs)

    reading.cache # Cache reading until it's saved
    Stats.new(reading) # calculate and cache thermostat metrics
    reading.delay.save! # Schedule db register

    reading
  end

  after_initialize :set_tracking_number # Initialize with proper number
  after_create :remove_cache # Remove cached reading when fully created

  # Asks thermostat for the next available number if not set yet
  def set_tracking_number
    self.tracking_number ||= thermostat.try(:next_tracking_number)
  end

  def remove_cache
    $redis.del(key)
  end

  def key
    thermostat.try(:reading_key, tracking_number)
  end

  def cache
    $redis.set(key, self.to_json)
  end
end
