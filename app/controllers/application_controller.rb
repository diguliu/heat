class ApplicationController < ActionController::API
  private

  def validates_household_token
    if params[:household_token].blank?
      render json: 'Missing household token to authenticate thermostat.', status: :unauthorized
    else
      @thermostat = Thermostat.find_by_household_token(params[:household_token])
      if @thermostat.blank?
        render json: 'No thermostat found with the provided household token.', status: :not_found
      end
    end
  end
end
