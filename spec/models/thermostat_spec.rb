require 'rails_helper'
require 'redis_helper'

RSpec.describe Thermostat, type: :model do
  let(:thermostat) { Thermostat.new }

  describe '#save' do
    it 'creates a random household_token' do
      thermostat.save!
      expect(thermostat.household_token).to_not be_nil
    end
  end

  describe '#get_reading' do
    let(:reading) { Reading.new(thermostat: thermostat) }

    it 'reads cache if not available on db yet' do
      reading.cache

      expect(Reading).to receive(:get_cached).with(reading.key).and_return(reading.to_json)
      thermostat.get_reading(reading.tracking_number)
    end

    it 'does not read cache when available on db' do
      reading.save!

      expect(Reading).not_to receive(:get_cached).with(reading.key)
      thermostat.get_reading(reading.tracking_number)
    end
  end

  describe '#last_tracking_number' do
    it 'defaults to 0 if no readings' do
      thermostat = Thermostat.new
      expect(thermostat.last_tracking_number).to eq(0)
    end

    it 'max readings tracking_number' do
      thermostat = Thermostat.create!
      Reading.create!(thermostat: thermostat)
      Reading.create!(thermostat: thermostat)

      expect(thermostat.last_tracking_number).to eq(2)
    end
  end

  describe '#next_tracking_number' do
    it 'defaults to 1 if no readings' do
      expect(thermostat.next_tracking_number).to eq(1)
    end

    it 'set number on cache' do
      thermostat.next_tracking_number
      expect($redis.get(thermostat.tracking_key)).to eq('1')
    end

    it 'return cached number without database fetching' do
      thermostat.next_tracking_number

      expect(thermostat).to_not receive(:last_tracking_number)
      thermostat.next_tracking_number
    end
  end
end
