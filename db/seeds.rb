10.times  do
  t = Thermostat.create({address: Faker::Address.full_address})
  5.times do
    Reading.create!({
      thermostat: t,
      temperature: Faker::Number.decimal(l_digits: 2),
      humidity: Faker::Number.decimal(l_digits: 2),
      battery_charge: Faker::Number.decimal(l_digits: 2)
    })
  end
end
