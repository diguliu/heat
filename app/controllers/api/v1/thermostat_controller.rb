module Api::V1
  class ThermostatController < ApplicationController
    before_action :validates_household_token, only: [:stats]

    def index
      thermostats = Thermostat.order('created_at DESC');
      render json: thermostats, status: :ok
    end

    def stats
      render json: Stats.report(@thermostat), status: :ok
    end
  end
end
