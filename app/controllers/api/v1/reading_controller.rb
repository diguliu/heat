module Api::V1
  class ReadingController < ApplicationController
    before_action :validates_household_token, only: [:show, :create]
    before_action :validates_reading, only: [:show]
    before_action :validates_fields, only: [:create]

    def index
      readings = Reading.order('created_at DESC');
      render json: readings, status: :ok
    end

    def show
      render json: @reading, status: :ok
    end

    def create
      reading = Reading.scheduled_create!(
        thermostat: @thermostat,
        temperature: params[:temperature],
        humidity: params[:humidity],
        battery_charge: params[:battery_charge]
      )

      render json: reading.tracking_number, status: :ok
    end

    private

    def is_number? string
      true if Float(string) rescue false
    end

    def validates_reading
      @reading = @thermostat.get_reading(params[:id])
      if @reading.blank?
        render json: 'No reading found with the provided tracking number.', status: :not_found
      end
    end

    def validates_fields
      Stats.fields.each do |field|
        if params[field].present?
          if !is_number?(params[field])
            render json: "Provided #{field} is not a float number.", status: :bad_request
            return
          end
        end
      end
    end
  end
end
