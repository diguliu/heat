require 'sidekiq/testing'
require 'rails_helper'
require 'redis_helper'
require 'sidekiq_helper'

RSpec.describe Api::V1::ReadingController, type: :controller do

  let(:thermostat) { Thermostat.create! }

  describe "GET #show" do
    it "shows created reading" do
      reading = Reading.create!(thermostat: thermostat)

      get :show, params: {
        id: reading.tracking_number,
        household_token: thermostat.household_token
      }

      expect(response).to have_http_status(:success)
      expect(parsed_response['thermostat_id']).to eq(thermostat.id)
      expect(parsed_response['tracking_number']).to eq(reading.tracking_number)
      expect(parsed_response['temperature']).to eq(reading.temperature)
      expect(parsed_response['humidity']).to eq(reading.humidity)
      expect(parsed_response['battery_charge']).to eq(reading.battery_charge)
    end

    it "shows cached reading while reading is not ready" do
      temperature = 11.11
      humidity = 22.22
      battery_charge = 33.33

      post :create, params: {
        temperature: temperature,
        humidity: humidity,
        battery_charge: battery_charge,
        household_token: thermostat.household_token
      }

      # No sidekiq jobs drain to create reading
      tracking_number = parsed_response

      get :show, params: {
        id: tracking_number,
        household_token: thermostat.household_token
      }

      parsed_response(true) # Reload response

      expect(parsed_response['id']).to be_nil # Reading not created yet
      expect(parsed_response['thermostat_id']).to eq(thermostat.id)
      expect(parsed_response['temperature']).to eq(temperature)
      expect(parsed_response['humidity']).to eq(humidity)
      expect(parsed_response['battery_charge']).to eq(battery_charge)
    end

    it 'warns missing household_token' do
      get :show, params: { id: 1 }
      expect(response).to have_http_status(:unauthorized)
    end

    it 'warns invalid household_token' do
      get :show, params: { id: 1, household_token: 'invalid-token'}
      expect(response).to have_http_status(:not_found)
    end

    it 'warns invalid tracking_number' do
      get :show, params: {id: 999,
        household_token: thermostat.household_token
      }
      expect(response).to have_http_status(:not_found)
    end
  end

  describe "POST #create" do
    it "posts new reading" do
      temperature = 11.11
      humidity = 22.22
      battery_charge = 33.33

      post :create, params: {
        temperature: temperature,
        humidity: humidity,
        battery_charge: battery_charge,
        household_token: thermostat.household_token
      }

      Sidekiq::Worker.drain_all

      expect(response).to have_http_status(:success)
      expect(parsed_response).to eq(1)

      reading = thermostat.get_reading(1)
      expect(reading.temperature).to eq(temperature)
      expect(reading.humidity).to eq(humidity)
      expect(reading.battery_charge).to eq(battery_charge)
    end

    it 'warns about invalid fields' do
      post :create, params: {
        temperature: 11.22,
        humidity: 22.22,
        battery_charge: 'invalid-field',
        household_token: thermostat.household_token
      }
      expect(response).to have_http_status(:bad_request)
    end
  end
end
