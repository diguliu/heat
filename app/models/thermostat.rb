class Thermostat < ApplicationRecord
  has_secure_token :household_token
  has_many :readings, dependent: :destroy

  # Tries to get reading from the db first. If not available yet then gets the
  # cached version
  def get_reading(tracking_number)
    readings.find_by_tracking_number(tracking_number) ||
      Reading.get_cached(reading_key(tracking_number))
  end

  def reading_key(tracking_number)
    "t#{id}-reading-#{tracking_number}"
  end

  def tracking_key
    "t#{id}-last-tracking-number"
  end

  def last_tracking_number
    readings.maximum(:tracking_number) || 0
  end

  # Tries to provide the next tracking number from cache. If not available yet
  # gets the last one used on the db. Then caches the number after getting it.
  def next_tracking_number
    tracking_number = ($redis.get(tracking_key) || last_tracking_number).to_i + 1
    $redis.set(tracking_key, tracking_number)
    tracking_number
  end
end
