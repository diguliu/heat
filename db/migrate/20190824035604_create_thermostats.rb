class CreateThermostats < ActiveRecord::Migration[6.0]
  def change
    create_table :thermostats do |t|
      t.string :household_token
      t.string :address

      t.timestamps
    end
    add_index :thermostats, :household_token, unique: true
  end
end
