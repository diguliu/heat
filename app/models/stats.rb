class Stats
  class << self
    def fields
      %w(temperature humidity battery_charge)
    end

    def metrics
      %w(min max avg)
    end

    # Incremental average
    # http://jvminside.blogspot.com/2010/01/incremental-average-calculation.html
    #
    # Calculating the incremental average allows us to register only the last
    # average instead of the full list of values for each field. Then upon
    # every new reading we just need to update the value.
    def inc_avg(current, n, v)
      current = 0 if current.nil?
      current + (v.to_f-current) / n
    end

    # Namespaced stats for the thermostats
    #
    # The stats are cached on redis in a pseudo hash structure. The structure
    # would look like this:
    # {
    #   thermostat1: { temperature: { min: 0, max: 100, avg: 50 }, ... },
    #   thermostat2: { temperature: { min: 20, max: 90, avg: 65 }, ... },
    #   ...
    # }
    #
    # To achieve that we build the keys' names following the logic below:
    #
    # t1-n: number of readings of thermostat with id 1
    # t2-temperature-max: max temperature read from thermostat with id 2
    # t12-battery_charge-avg: average battery_charge read from thermostat with id 12
    #
    # And so on. This enables a O(1) complexity to retrieve any metric.
    def stats
      @@stats ||= Redis::Namespace.new(:stats, :redis => Redis.new)
    end

    def get(key)
      stats.get(key)
    end

    def set(key, value)
      stats.set(key, value)
    end

    # Gets number of readings
    def get_n(thermostat)
      get("t#{thermostat.id}-n").to_i
    end

    # Sets number of readings
    def set_n(thermostat, n)
      set("t#{thermostat.id}-n", n)
    end

    # Initialize/Increment number of readings
    def update_n(thermostat)
      value = get_n(thermostat) + 1
      set_n(thermostat, value)
    end

    # Gets metric of thermostat field
    def get_metric(thermostat, field, metric)
      value = get("t#{thermostat.id}-#{field}-#{metric}")
      value.present? ? value.to_f : nil
    end

    # Sets metric of thermostat field
    def set_metric(thermostat, field, metric, value)
      set("t#{thermostat.id}-#{field}-#{metric}", value)
    end

    # Checks for new min or max and calculates new avg
    def update_field(reading, field)
      thermostat = reading.thermostat
      value = reading.send(field)
      current_min = get_metric(thermostat, field, :min)
      if current_min.nil? || (value < current_min)
        set_metric(thermostat, field, :min, value)
      end

      current_max = get_metric(thermostat, field, :max)
      if current_max.nil? || (value > current_max)
        set_metric(thermostat, field, :max, value)
      end

      current_n = get_n(thermostat)
      current_avg = get_metric(thermostat, field, :avg)
      new_avg = inc_avg(current_avg, current_n, value)
      set_metric(thermostat, field, :avg, new_avg)
    end

    # Register stats of new reading
    def new(reading)
      update_n(reading.thermostat)

      fields.each do |field|
        update_field(reading, field)
      end
    end

    # Load cache for every thermostat and theirs readings
    def load
      Thermostat.all.includes(:readings).each do |thermostat|
        thermostat.readings.each do |reading|
          new(reading)
        end
      end
    end

    # Clear stats cache. This has to be done manually to avoid flushing other
    # non-related caches.
    def flush
      keys = []
      Thermostat.all.each do |t|
        keys << "t#{t.id}-n"
        fields.each do |f|
          metrics.each do |m|
            keys << "t#{t.id}-#{f}-#{m}"
          end
        end
      end

      stats.del(keys) if keys.present?
    end

    def reload
      flush
      load
    end

    # Builds stats hash based on cached values for the thermostat
    def report(thermostat)
      result = {}

      fields.each do |field|
        result[field] = {}
        metrics.each do |metric|
          result[field][metric] = get_metric(thermostat, field, metric)
        end
      end

      result
    end
  end
end
