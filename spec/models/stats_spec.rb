require 'rails_helper'
require 'redis_helper'

RSpec.describe Stats, type: :model do
  let(:thermostat) { Thermostat.create! }
  let(:value) { rand(100) }
  let(:field) { Stats.fields.sample }
  let(:metric) { Stats.metrics.sample }

  describe '.inc_avg' do
    it 'calculates incremental average for first element' do
      expect(Stats.inc_avg(nil, 1, 5)).to eq(5)
    end

    it 'calculates n+1 incremental average' do
      elements = [1, 6, 9, 3, 20]
      average = elements.inject(0) { |sum, n| sum + n }.to_f / elements.size

      new_element = 8
      elements << new_element
      new_average = elements.inject(0) { |sum, n| sum + n }.to_f / elements.size

      expect(Stats.inc_avg(average, elements.size, new_element))
        .to eql(new_average)
    end
  end

  it 'sets and gets stat' do
    key = 'some-key'
    Stats.set(key, value)
    expect(Stats.get(key)).to eql(value.to_s)
  end

  it 'sets and gets thermostat number of readings' do
    Stats.set_n(thermostat, value)
    expect(Stats.get_n(thermostat)).to eql(value)
  end

  describe '.update_n' do
    it 'defaults to 1' do
      Stats.update_n(thermostat)
      expect(Stats.get_n(thermostat)).to eql(1)
    end

    it 'increment by 1' do
      Stats.set_n(thermostat, value)
      Stats.update_n(thermostat)
      expect(Stats.get_n(thermostat)).to eql(value + 1)
    end
  end

  it 'sets and gets thermostat field metric in float' do
    Stats.set_metric(thermostat, field, metric, value)
    expect(Stats.get_metric(thermostat, field, metric)).to eql(value.to_f)
  end

  describe '.update_field' do
    it 'defaults metrics to first reading' do
      reading = Reading.new(thermostat: thermostat)
      reading.send("#{field}=", value)

      Stats.new(reading)

      expect(Stats.get_metric(thermostat, field, :min)).to eql(value.to_f)
      expect(Stats.get_metric(thermostat, field, :max)).to eql(value.to_f)
      expect(Stats.get_metric(thermostat, field, :avg)).to eql(value.to_f)
    end

    it 'update min' do
      r1 = Reading.new(thermostat: thermostat)
      r1.send("#{field}=", value)
      Stats.new(r1)

      r2 = Reading.new(thermostat: thermostat)
      new_value = value - rand(10) 
      r2.send("#{field}=", new_value)
      Stats.new(r2)

      expect(Stats.get_metric(thermostat, field, :min)).to eql(new_value.to_f)
      expect(Stats.get_metric(thermostat, field, :max)).to eql(value.to_f)
    end

    it 'update max' do
      r1 = Reading.new(thermostat: thermostat)
      r1.send("#{field}=", value)
      Stats.new(r1)

      r2 = Reading.new(thermostat: thermostat)
      new_value = value + rand(10) 
      r2.send("#{field}=", new_value)
      Stats.new(r2)

      expect(Stats.get_metric(thermostat, field, :min)).to eql(value.to_f)
      expect(Stats.get_metric(thermostat, field, :max)).to eql(new_value.to_f)
    end

    it 'update avg' do
      r1 = Reading.new(thermostat: thermostat)
      r1.send("#{field}=", value)
      Stats.new(r1)

      r2 = Reading.new(thermostat: thermostat)
      new_value = value + rand(10) 
      r2.send("#{field}=", new_value)
      Stats.new(r2)

      expect(Stats.get_metric(thermostat, field, :avg))
        .to eql(Stats.inc_avg(value, 2, new_value).to_f)
    end
  end

  it '.new' do
    reading = Reading.new(thermostat: thermostat)

    expect(Stats).to receive(:update_n).with(thermostat)
    Stats.fields.each do |field|
      expect(Stats).to receive(:update_field)
        .with(reading, field)
    end

    Stats.new(reading)
  end

  it '.report' do
    Reading.scheduled_create!(
      thermostat: thermostat,
      temperature: 10,
      humidity: 20,
      battery_charge: 30
    )

    Reading.scheduled_create!(
      thermostat: thermostat,
      temperature: 20,
      humidity: 30,
      battery_charge: 10
    )

    Reading.scheduled_create!(
      thermostat: thermostat,
      temperature: 30,
      humidity: 10,
      battery_charge: 20
    )

    report = Stats.report(thermostat)

    Stats.fields.each do |field|
      expect(report[field]['min']).to eq(10)
      expect(report[field]['max']).to eq(30)
      expect(report[field]['avg']).to eq(20)
    end
  end
end
