Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      resources :thermostat, only: [:index]
      get 'thermostat/stats'

      resources :reading, only: [:index, :show, :create]
    end
  end
end
