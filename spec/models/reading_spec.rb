require 'rails_helper'
require 'redis_helper'

RSpec.describe Reading, type: :model do
  describe '#create' do
    let(:t1) { Thermostat.create! }
    let(:t2) { Thermostat.create! }

    it 'sets 1 as the first tracking_number by household' do
      r1 = Reading.create!(thermostat: t1)
      expect(r1.tracking_number).to eq(1)

      r2 = Reading.create!(thermostat: t2)
      expect(r2.tracking_number).to eq(1)
    end

    it 'increment tracking_number by household' do
      r11 = Reading.create!(thermostat: t1)
      r12 = Reading.create!(thermostat: t1)
      expect(r11.tracking_number).to eq(1)
      expect(r12.tracking_number).to eq(2)

      r21 = Reading.create!(thermostat: t2)
      r22 = Reading.create!(thermostat: t2)
      expect(r21.tracking_number).to eq(1)
      expect(r22.tracking_number).to eq(2)
    end
  end

  describe '#cache' do
    let(:thermostat) { Thermostat.create! }
    let(:reading) { Reading.new(thermostat: thermostat) }

    it 'cache reading' do
      reading.cache
      expect($redis.get(reading.key)).to eq(reading.to_json)
    end

    it 'removes cache after creation' do
      reading.cache

      reading.save!
      expect($redis.get(reading.key)).to be_nil
    end
  end
end
