module HeatHelper
  def parsed_response(reload = false)
    if reload
      @parsed_response = JSON.parse(response.body)
    else
      @parsed_response ||= JSON.parse(response.body)
    end
  end
end
